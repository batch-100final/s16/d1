 
// 1. Arrow Function Expression

// Before ES6, function expression syntax:

function name(parameters){
	statements
}

// function declaration
// 	name - function name
// 	param - placeholder / the name of an argument to be passed to the function
// 	statements - function body 

// example of normal function

function generateFullName(firstName, middleInitial, lastName) {
	return firstName + " " + middleInitial + ". " + lastName;
}

//in ES6, arrow function expression syntax:
() =>{
	statements
}

const variableName = () => {

}

//example of arrow function expression

const generateFullName(firstName,middleInitial,lastName) => {
	return firstName + " " + middleInitial + ". " +lastName;
}

//additional in arrow function curl brackets can be ommited but still can return a value

//syntax:
const add = (x, y) => x + y

//longer version
const add = (x, y) => {
	return x + y;
}

//NOTE: when do we use arrow function
//it can be used when you do not need to name a function
//the function has a simple task to do

//we will use the normal function
let numbers = [1, 2, 3, 4, 5]

let numbersMap = numbers.map(function(number){
	return number * number
});

console.log(numbersMap)

//in arrow function

let numbersMap = numbers.map(number => number * number);
console.log(numbersMap)

//2 template literals
// are string literals allowing embedded expression (concatination)

const generateFullName(firstName,middleInitial,lastName) => {
	return firstName + " " + middleInitial + ". " +lastName;
}

//syntax:
`string text` 
`string text line 1
 string text line 2`

 `string text ${expression} string text`
 //Template literals can contain placeholders. we used dollar sign and curly brackets. The expressions in the placeholders and the text between the backtick get passed to a function

//concatination
`${expression}, ${expression}`

console.log(`string text line 1
			string text line 2`)

const generateFullName = (firstName, middleInitial, lastName) =>{
	return `${firstName} ${middleInitial}. ${lastName}`
}

//can also do computations or evaluations inside:
`${8 * 6}`
const add = (x,y) => {
	return x+y
}

//the sum is
const add = (x, y) => `the sum is ${x + y}`;
console.log(add)

//3. DESTRUCTURING ARRAYS AND OBJECTS
//destructuring assignments syntax is a js expression that makes it possible to unpack values from arrays, or properties from objects, into distinct variables.

//normal array
let num = [10, 20]
let num = `${num[0]} ${num[1]}`

//destructuring assignments

let a, b, rest;

[a, b] = [10, 20]

console.log(a)
console.log(b)

[a, b, ...rest] = [10, 20, 30, 40, 50]
console.log(rest)

//example, we have an array that contains the name of a person. If we want to combine it to a full name, we will need to do it like this:
let name = ["Juan", "Dela", "Cruz"];
let fullName = `${name[0]} ${name[1]} ${name[2]}`

[fName, mName, lName] = ["Juan", "Dela", "Cruz"];
console.log(`${fName} ${mName} ${lName}`)

or

[fName, mName, lName] = ["Juan", "Dela", "Cruz"];

let fullName = `${fName} ${mName} ${lName}`
console.log(fullName)


//normal object

const person = {
	firstName: "Juan",
	middleName: "Dela",
	lastName: "Cruz"
}

const generateFullName = (firstName, middleName, lastName) => {
	return `${firstName} ${middleName} ${lastName}`
}

generateFullName(person.firstName, person.middleInitial, person.lastName);


//destructuring object
const person = {
	firstName: "Juan",
	middleName: "Dela",
	lastName: "Cruz"
}

const generateFullName = ({firstName, middleName, lastName}) =>{
	return `${firstName} ${middleName} ${lastName}`
}

generateFullName(person);














